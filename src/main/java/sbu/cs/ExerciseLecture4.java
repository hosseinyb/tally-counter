package sbu.cs;

import java.util.Locale;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long temp = 1;
        while (n > 0) {
            temp *= n;
            n--;
        }
        return temp;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        String answer = "";
        for (int i = 0; i < word.length(); i++) {
            answer = word.charAt(i) + answer;
        }
        return answer;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        line = line.replaceAll(" ", "");
        line = line.toLowerCase();
        String part1 = line.substring(0, (line.length() / 2));
        String part2;
        if (line.length() % 2 != 0) {
            part2 = line.substring((line.length() / 2 + 1), line.length());
        } else {
            part2 = line.substring((line.length() / 2), line.length());
        }
        if (part1.equals(reverse(part2))) {
            return true;
        }
        return false;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   a
     *   l       * *
     *   i
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str2, String str1) {
        char[][] answer = new char[str2.length()][str1.length()];
        for (int i = 0; i < str2.length(); i++) {
            for (int j = 0; j < str1.length(); j++) {
                if (str2.charAt(i) == str1.charAt(j)) {
                    answer[i][j] = '*';
                }
                else {
                    answer[i][j] = ' ';
                }
            }
        }
        return answer;
    }
}
