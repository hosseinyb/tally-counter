package sbu.cs;

import java.util.Scanner;

public class TallyCounter implements TallyCounterInterface {
    private int length = 0;

    @Override
    public void count() {
        if (length < 9999) {
            length++;
        }
    }

    @Override
    public int getValue() {
        return length;
    }
    @Override
    public void setValue(int value) {
        if (value <  0 || value > 9999) {
            throw new IllegalValueException();
        }
        length = value;
    }
}
