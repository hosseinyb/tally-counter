package sbu.cs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long answer = 0;
        int i = 0;
        while (i < arr.length) {
            answer += arr[i];
            i += 2;
        }
        return answer;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int f = 0, e = arr.length - 1;
        while (f < arr.length / 2) {
            int temp = arr[f];
            arr[f] = arr[e];
            arr[e] = temp;
            f++;
            e--;
        }
        return arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int m2_C = m2[0].length, m1_C = m1[0].length, m2_R = m2.length, m1_R = m1.length;
        double[][] answer = new double[m1_R][m2_C];
        if (m1_C != m2_R) {
            throw new RuntimeException();
        }
        for (int i = 0; i < m1_R; i++) {
            for (int j = 0; j < m2_C; j++) {
                for (int f = 0; f < m1_C; f++) {
                    answer[i][j] += m1[i][f] * m2[f][j];
                }
            }
        }
        return answer;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> answer = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            ArrayList<String> temp = new ArrayList<String>();
            for (int j = 0; j < names[0].length; j++) {
                temp.add(names[i][j]);
            }
            answer.add(temp);
        }
        return answer;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> answer = new ArrayList<Integer>();
        for (int i = 2; i <= n; i++) {
            if ((n % i) == 0) {
                answer.add(i);
                while ( (n % i) == 0) {

                    n = n / i;
                }
            }
        }
        return answer;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> answer = new ArrayList<String>();
        line = line.replaceAll("[^a-zA-Z0-9]" ," ");
        String word = "";
        for (int i = 0; i < line.length(); i++) {
            if ( line.charAt(i) == ' ' && word != "") {
                word = word.replaceAll(" ","");
                answer.add(word);
                word = "";
                continue;
            }
            word = word + line.charAt(i);
        }
        for (String n: answer) {
            System.out.println(n);
        }
        return answer;
    }
}
