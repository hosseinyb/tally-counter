package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        char[] leter = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        Random rnd = new Random();
        String answer = "";
        for (int i = 0; i < length; i++) {
            answer = answer + leter[rnd.nextInt(leter.length)];
        }
        return answer;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {         //-------------------------------------------
        if (length < 3) {
            throw new IllegalValueException();
        }
        String answer = "";
        char[] difrentDigits = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        char[] number = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char[] SPchar = {'@', '!', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '.', '<', ',', '>', '/', '?', '\\', '|', '\'', '\"', ':', ';', '{', '}', '[', ']', '~', '`'};
        Random rnd = new Random();

        for (int i = 1; i <= (length-2); i++) {
            answer = answer + difrentDigits[rnd.nextInt(difrentDigits.length)];
        }
        answer = answer + number[rnd.nextInt(number.length)];
        answer = answer + SPchar[rnd.nextInt(SPchar.length)];
        return answer;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        ExerciseLecture4 temp1 = new ExerciseLecture4();
        int i = 1;
        while (true) {
            String bin_S = Integer.toBinaryString((int) temp1.fibonacci(i));
            int bin_I = 0;
            for (int j = 0; j < bin_S.length(); j++) {
                if (bin_S.charAt(j) == '1') {
                    bin_I += 1;
                }
            }
            if ((temp1.fibonacci(i) + bin_I) == n) {
                return true;
            }
            if ((temp1.fibonacci(i) + bin_I) > n) {
                break;
            }
            i++;
        }
        return false;
    }
}
